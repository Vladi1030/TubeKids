Post users

{
    "name": "Carlos",
    "lastname": "Rojas",
    "email": "cr2000@gmail.com",
    "password": "cr1234",
    "password_ver": "cr1234",
    "country": "Costa Rica",
    "birthday": "10/01/2000"
}

patch

{
	"newinfo": {
			"name": "Pop"
	}
}

Database

User(email, password, name, lastname, country, birthday);
Playlist(name, author);
Profile(name, username, code, age, parental_control);
Video(name, path, playlist, type_of_video);

User --> birthday = Date(years > 18)
Playlist --> author = User ID(user reference)
Profile --> parental_control = User ID(user reference)
Video --> type_of_video = Boolean(true = local reference, false = URL)