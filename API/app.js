//Required things
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();
const cors = require('cors');
const jwt = require('jsonwebtoken');
const config = require('./config');
const bodyParser = require('body-parser');
const validator = require("email-validator");
const btoa = require('btoa');
const atob = require('atob');
const nodemailer = require('nodemailer');

// Models
const User = require('./models/user');
const Video = require('./models/video');
const Profile = require('./models/profile');
const Playlist = require('./models/playlist');

//API port
const port = process.env.PORT || 3000;

//database
const db = mongoose.connect(config.database, {
    useNewUrlParser: true
});

//Functional things
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));
app.use(cors({
    origin: '*'
}));
app.all('*', cors());

//Listening
app.listen(3000, () => console.log('TubeKids API is listening on port 3000!'));

//'root' request
app.get('/', function(req, res) {
    res.send('Hi! The API is at http://localhost:' + port + '/api');
});

//Api root request
app.get('/api/', function(req, res) {
    res.send('Hi! Please, specify the section http://localhost:' + port + '/api' + '/anysection');
});

//transporter for emails
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'projecttestsutn@gmail.com',
        pass: 'ProjectTests1234'
    }
});

//Users post
app.post('/api/users', (req, res) => {
    let user = new User();
    let access = true;

    user.name = req.body.name;
    user.lastname = req.body.lastname;
    user.email = req.body.email;
    user.password = btoa(req.body.password);
    user.country = req.body.country;
    user.birthday = req.body.birthday;
    user.verified = false;
    let password_ver = btoa(req.body.password_ver);

    if (!validator.validate(user.email)) {
        access = false;
    } else if (user.name === '' || user.lastname === '' || user.country === '') {
        access = false;
    } else if (user.password != password_ver || password_ver === '') {
        access = false;
    } else if (calculateAge(user.birthday) < 18) {
        access = false;
    }

    User.findOne({
        email: user.email
    }, function(err, userver) {
        if (userver != null) {
            res.status(400);
            res.json("An account with this email already exist!");
            return;
        } else {
            if (access) {
                user.save(function(err) {
                    if (err) {
                        res.status(200);
                        res.json({
                            error: err
                        });
                        return;
                    }
                    res.status(201);
                    res.json(user);
                    initEmail(user);
                });
            } else {
                res.status(400);
                res.json("Information needed");
                return;
            }
        }
    });
});

//Users get
app.get('/api/users', (req, res) => {
    /*let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }*/

    User.find(function(err, users) {
        if (err) {
            res.status(400);
            res.send(err);
        }
        res.status(200);
        res.json(users);
    });
});

//User by id
app.get('/api/users/:id', function(req, res) {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            User.findById(req.params.id, function(err, users) {
                if (err) {
                    res.status(404);
                    res.send("User not found");
                }
                res.status(200);
                res.send(users);
            })
        }
    }
})

//User delete
app.delete('/api/users/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            User.findOneAndRemove({
                _id: req.params.id
            }, (err) => {
                if (err) {
                    res.status(404);
                    res.send("User not found");
                }
                res.status(200);
                res.json("User deleted");
            });
        }
    }
});

//User patch
app.patch('/api/users/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Users.findOneAndUpdate({
                _id: req.params.id
            }, {
                $set: req.body.newinfo
            }, {
                new: true
            }, function(err, user) {
                if (err) {
                    res.status(400);
                    res.send("Something wrong when updating data!");
                }
                res.status(201);
                res.send("Updated");
            });
        }
    }
});

//Auth user
app.post('/api/authenticate', function(req, res) {

    // find the user
    User.findOne({
        email: req.body.email
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            res.status(400);
            res.json({
                success: false,
                message: 'Authentication failed. User not found.'
            });
        } else if (user) {
            if (user.verified) {
                // check if password matches
                if (user.password != btoa(req.body.password)) {
                    res.status(400);
                    res.json({
                        success: false,
                        message: 'Authentication failed. Wrong information.'
                    });
                } else {

                    // if user is found and password is right
                    // create a token with only our given payload
                    // we don't want to pass in the entire user since that has the password
                    let payload = {
                        id: user._id,
                        email: user.email,
                        password: user.password,
                        name: user.name,
                        lastname: user.lastname,
                        country: user.country,
                        birthday: user.birthday,
                        user_type: 'user'
                    };

                    var token = jwt.sign(payload, config.secret, {
                        expiresIn: 86400
                    });

                    // return the information including token as JSON
                    res.status(200);
                    res.json({
                        success: true,
                        user_type: "user",
                        message: 'Enjoy your token!',
                        token: token
                    });
                }
            } else {
                res.status(400);
                res.json('You need to verify your e-mail address first, check out your email!');
            }
        }

    });
});

//Verify
app.post('/api/users/verify', function(req, res) {
    let senttoken = req.body.verifytoken;
    let password_from = senttoken.split("zxc.23").pop();
    let email_from = atob(senttoken.split('zxc.23').shift());

    // find the user
    User.findOne({
        email: email_from
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            res.status(400);
            res.json({
                success: false,
                message: 'Authentication failed. User not found.'
            });
        } else if (user) {

            // check if password matches
            if (user.password != password_from) {
                res.status(400);
                res.json({
                    success: false,
                    message: 'Verification failed. Wrong information.'
                });
            } else {
                user.verified = true;

                user.save(function(err, verifiedUser) {
                    if (err) return handleError(err);
                    user = verifiedUser;
                });

                let payload = {
                    id: user._id,
                    email: user.email,
                    password: user.password,
                    name: user.name,
                    lastname: user.lastname,
                    country: user.country,
                    birthday: user.birthday,
                    verified: user.verified,
                    user_type: 'user'
                };

                var token = jwt.sign(payload, config.secret, {
                    expiresIn: 86400
                });

                // return the information including token as JSON
                res.status(200);
                res.json({
                    success: true,
                    user_type: "user",
                    message: 'Enjoy your token!',
                    token: token
                });
            }
        }
    });
});

//Videos post
app.post('/api/videos', (req, res) => {
    let video = new Video();
    let access = true;

    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    video.type_of_video = req.body.type_of_video;

    if (video.type_of_video) {
        let file = req.params.file;
        let fileLocation = path.join('./uploads', file);
        res.download(fileLocation, file);
        video.path = fileLocation
    } else {
        var match = req.body.path.split("v=").pop();
        if (match.length != 11) {
            res.status(400);
            res.json({
                error: 'The URL is not valid!'
            });
            return;
        }
        video.path = '//www.youtube.com/embed/' + match;
    }

    video.name = req.body.name;
    video.playlist = req.body.playlist;

    if (video.name === '' || video.path === '' || video.playlist === null) {
        access = false;
    }

    Playlist.findById(video.playlist, function(err, playlist) {
        if (err) {
            access = false;
        }
    })

    if (access) {
        video.save(function(err) {
            if (err) {
                res.status(422);
                res.json({
                    error: err
                });
            }
            res.status(201);
            res.json(video);
        });
    } else {
        res.status(400);
        res.json("Information needed");
    }
});

//Videos get
app.get('/api/videos', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    Video.find(function(err, videos) {
        if (err) {
            res.status(400);
            res.send(err);
        }
        res.status(200);
        res.json(videos);
    });
});

//Video by id
app.get('/api/playlists/:id', function(req, res) {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Video.findById(req.params.id, function(err, video) {
                if (err) {
                    res.status(400);
                    res.send("Video not found");
                }
                res.status(200);
                res.send(video);
            })
        }
    }
})

//Video delete
app.delete('/api/videos/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Video.findOneAndRemove({
                _id: req.params.id
            }, (err) => {
                if (err) {
                    res.status(400);
                    res.send("Video not found");
                }
                res.status(201);
                res.json("Video deleted");
            });
        }
    }
});

//Profiles post
app.post('/api/profiles', (req, res) => {
    let profile = new Profile();
    let access = true;

    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    profile.name = req.body.name;
    profile.username = req.body.username;
    profile.code = req.body.code;
    profile.age = req.body.age;
    profile.parental_control = req.body.parental_control;

    if (profile.name === '' || profile.username === '' || profile.code === '') {
        access = false;
    } else if (profile.age === null) {
        access = false;
    } else if (profile.parental_control === null) {
        access = false;
    }

    User.findById(profile.parental_control, function(err, user) {
        if (err) {
            access = false;
        }
    })

    Profile.findOne({
        username: profile.username,
        parental_control: profile.parental_control
    }, function(err, oldProfile) {
        if (oldProfile != null) {
            res.status(400);
            res.json("A profile with this username and this parental_control already exist!");
        } else {
            if (access) {
                profile.save(function(err) {
                    if (err) {
                        res.status(422);
                        res.json({
                            error: err
                        });
                    }
                    res.status(201);
                    res.json(profile);
                });
            } else {
                res.status(400);
                res.json("Information needed");
            }
        }
    });
});

//Profiles get
app.get('/api/profiles', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    Profile.find(function(err, profiles) {
        if (err) {
            res.status(400);
            res.send(err);
        }
        res.status(200);
        res.json(profiles);
    });
});

//Profile by id
app.get('/api/profiles/:id', function(req, res) {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Profile.findById(req.params.id, function(err, profile) {
                if (err) {
                    res.status(400);
                    res.send("Profile not found");
                }
                res.status(200);
                res.send(profile);
            })
        }
    }
})

//Profile delete
app.delete('/api/profiles/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Profile.findOneAndRemove({
                _id: req.params.id
            }, (err) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                }
                res.status(200);
                res.json("Profile deleted");
            });
        }
    }
});

//Profile patch
app.patch('/api/profiles/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Profile.findOneAndUpdate({
                _id: req.params.id
            }, {
                $set: req.body.newinfo
            }, {
                new: true
            }, function(err, user) {
                if (err) {
                    res.status(400);
                    res.send("Something wrong when updating data!");
                }
                res.status(201);
                res.send("Updated");
            });
        }
    }
});

//Profile login
app.post('/api/profile/login', function(req, res) {

    // find the user
    Profile.findOne({
        username: req.body.username
    }, function(err, profile) {

        if (err) throw err;

        if (!profile) {
            res.status(400);
            res.json({
                success: false,
                message: 'Authentication failed. Profile not found.'
            });
        } else if (profile) {

            if (user.code != req.body.code) {
                res.status(400);
                res.json({
                    success: false,
                    message: 'Authentication failed. Wrong information.'
                });
            } else {
                const payload = {
                    name: profile.name,
                    username: profile.username,
                    code: profile.code,
                    age: profile.age,
                    parentel_control: profile.parental_control,
                    user_type: 'profile'
                };

                var token = jwt.sign(payload, config.secret, {
                    expiresIn: 86400
                });

                // return the information including token as JSON
                res.status(200);
                res.json({
                    success: true,
                    user_type: "profile",
                    message: 'Enjoy your token!',
                    token: token
                });
            }

        }

    });
});

//Playlists post
app.post('/api/playlists', (req, res) => {
    let playlist = new Playlist();
    let access = true;

    playlist.name = req.body.name;
    playlist.author = req.body.author;
    playlist.detail = req.body.detail;

    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (playlist.author === null) {
        access = false;
    } else {
        if (mongoose.Types.ObjectId.isValid(playlist.author)) {
            User.findOne({
                _id: playlist.author
            }, function(err, profile) {
                if (err) {
                    access = false;
                }
            });
        } else {
            access = false;
        }
    }

    Playlist.findOne({
        name: playlist.name,
        author: playlist.author
    }, function(err, oldPlaylist) {
        if (oldPlaylist != null) {
            res.status(400);
            res.json("A playlist with this name and this author already exist!");
        } else {
            if (access) {
                playlist.save(function(err) {
                    if (err) {
                        res.status(422);
                        res.json({
                            error: err
                        });
                    }
                    res.status(201);
                    res.json(playlist);
                });
            } else {
                res.status(400);
                res.json("Information needed");
            }
        }
    });
});

//Playlists get
app.get('/api/playlists', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    Playlist.find(function(err, playlists) {
        if (err) {
            res.status(400);
            res.send(err);
        }
        res.status(200);
        res.json(playlists);
    });
});

//Playlist by id
app.get('/api/playlists/:id', function(req, res) {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Playlist.findById(req.params.id, function(err, playlists) {
                if (err) {
                    res.status(400);
                    res.send("Playlist not found");
                }
                res.status(200);
                res.send(playlists);
            })
        }
    }
})

//Playlist delete
app.delete('/api/playlists/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Playlist.findOneAndRemove({
                _id: req.params.id
            }, (err) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                } //Required things
                res.status(200);
                res.json("Playlist deleted");
            });
        }
    }
});

//Playlist patch
app.patch('/api/playlists/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Wrong token!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Null pointer exception");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Playlist.findOneAndUpdate({
                _id: req.params.id
            }, {
                $set: req.body.newinfo
            }, {
                new: true
            }, function(err, playlist) {
                if (err) {
                    res.status(400);
                    res.send("Something wrong when updating data!");
                }
                res.status(200);
                res.send("Updated");
            });
        }
    }
});

//sends an email
var initEmail = function(user) {
    let verifytoken = btoa(user.email) + 'zxc.23' + user.password;
    let emailSent = false;
    let url = 'http://localhost:' + config.clientport + '/verify/' + verifytoken + ''
    var mailOptions = {
        from: 'projecttestsutn@gmail.com',
        to: user.email,
        subject: 'Hi ' + user.name + ', Welcome to TubeKids',
        html: '<center><a href=\'' + url + '\'><img src=\'http://cultipani.com/assets/images/tubekidsverification.jpg\'></img></a></center>',
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            emailSent = false;
        } else {
            emailSent = true;
        }
    });

    return emailSent;
};

//gets the years of an user
var calculateAge = function(birthday) {
    let now = new Date();
    let past = new Date(birthday);
    let nowYear = now.getFullYear();
    let pastYear = past.getFullYear();
    let age = nowYear - pastYear;

    return age;
};

//verifys the auth token
var verifyToken = function(token) {
    let verified = false;
    if (!token) {
        verified = false;
    }

    token = token.replace('Bearer ', '');

    jwt.verify(token, 'tubekidsapi', function(err) {
        if (err) {
            verified = false;
        } else {
            verified = true;
        }
    });

    return verified;
};