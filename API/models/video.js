const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const video = new Schema({
    name: {
        type: String,
        required: true
    },
    path: {
        type: String
    },
    playlist: {
        type: Schema.Types.ObjectId,
        ref: "playlist",
        required: true
    },
    type_of_video: {
        type: Boolean,
        required: true
    }
});

module.exports = mongoose.model('videos', video);
