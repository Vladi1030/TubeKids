const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const playlist = new Schema({
    name: {
        type: String,
        required: true
    },
    detail: {
        type: String
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: "user",
        required: true
    }
});

module.exports = mongoose.model('playlists', playlist);