const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profile = new Schema({
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    code: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    parental_control: {
        type: Schema.Types.ObjectId,
        ref: "user",
        required: true
    }
});

module.exports = mongoose.model('profiles', profile);
