const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    verified: {
        type: Boolean,
        required: true
    },
    birthday: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('users', user);